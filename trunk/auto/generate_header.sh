###
 # @Descripttion: 
 # @version: 
 # @Author: HuangJunren
 # @Date: 2020-08-04 14:38:08
 # @LastEditors: HuangJunren
 # @LastEditTime: 2020-08-04 15:06:25
### 
#!/bin/bash
# genereate the library header file.

objs=$1

rm -f $objs/include/srs_librtmp.h &&
cp $objs/../src/libs/srs_librtmp.hpp $objs/include/srs_librtmp.h
echo "genereate srs-librtmp headers success"
